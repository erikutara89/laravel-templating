@extends('layout.master')
@section('judul')
    Media Online
@endsection
@section('content')

<h1>Media Online</h1>
<h2>Sosial Media Developer</h2>
<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
<h3>Benefit belajar di Media Online</h3>

<ul>
<li>Mendapatkan motivasi dari sesama para Developer</li>
<li>Sharing Knowledge</li>
<li>Dibuat oleh calon web developer terbaik</li>
</ul>

<h3>Cara Bergabung Ke Media Online</h3>

<ol>
<li>Mengunjungi Website ini</li>
<li>mendaftarkan di <a href="/register">Form Sign Up</a>
</li>
<li>Selesai</li>
</ol>

@endsection